package tech.fusion.backend.calendarapi.model;

public class Location {
    public String id;
    public String name;

    @Override
    public String toString() {
        return "Location{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
