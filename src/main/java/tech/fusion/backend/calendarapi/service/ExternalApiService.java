package tech.fusion.backend.calendarapi.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import tech.fusion.backend.calendarapi.model.Location;

import java.util.Arrays;

@Service
public class ExternalApiService {
    private static final Logger log = LoggerFactory.getLogger(ExternalApiService.class);

    @Value("${external-api.url}")
    private String externalApiUrl;

    @Autowired
    private RestTemplate restTemplate;

    public Location getFirstLocation() {
        ResponseEntity<Location[]> response = restTemplate.getForEntity(
                externalApiUrl + "/api/locations",
                Location[].class
        );

        Location[] locations = response.getBody();

        if(locations != null && locations.length > 0) {
            log.info("Received: {}", Arrays.asList(locations));
            return locations[0];
        } else {
            return null;
        }
    }
}
