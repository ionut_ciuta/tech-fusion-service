package tech.fusion.backend.calendarapi.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.fusion.backend.calendarapi.model.Location;
import tech.fusion.backend.calendarapi.service.ExternalApiService;

@RestController
@RequestMapping("/api")
public class DataController {
    private static final Logger log = LoggerFactory.getLogger(DataController.class);

    @Autowired
    private ExternalApiService externalApiService;

    @GetMapping("/locations/random")
    public ResponseEntity<Location> getRandomLocation() {
        Location response = externalApiService.getFirstLocation();
        log.info("Random location: {}", response);

        if(response != null) {
            return ResponseEntity.ok(response);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // TODO: your new endpoints here
}
