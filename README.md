## Tech Fusion Codebase

Build the app using Maven
`mvn clean install`

Run via IntelliJ or directly in the terminal
`java -jar target/calendar-api.jar`

To check it works, in Postman or the browser access:

`localhost:9001/api/locations/random`

## How the app works
The app reads data from an API somewhere in AWS. I've already inserted some data in a MongoDB for you to play with.

The API you use exposes the following endpoints:
* /api/locations - returns a list of locations where events take place
* /api/owners - returns a list of owners
* /api/communities - returns a list of communities
* /api/events - returns the list of events
* /api/types - returns the list of types of events

(Disclaimer: this API is dumb, but that's not the point)

You will find the API host/address somewhere in the code. Spoiler: it's a YAML file ;)

Use that and the above API endpoints to see what data you get. Use your browser or POSTMAN for this.
All requests are GET.

## Requirements

Use the existing **DataController** as an example and add your new endpoints there.

All request should be GET.

You should get all necessary input using the **ExternalApiService**. All entities you receive have IDs.

Keep this in mind ;) you'll need them.

You'll receive dates in this format: yyyy-MM-dd'T'HH:mm:ss.SSSXXX

### 1. New endpoint to get most recent event
**URL: /api/events/most-recent**

Response structure:
- name
- description
- endDate hour, minutes, day, month, year
- ownerName
- eventId


### 2. New endpoint to get upcoming event
**URL: /api/events/upcoming**

Response structure:
- name
- description
- startDate, but just hour, minutes, day, month, year
- endDate hour, minutes, day, month, year
- typeName
- ownerName
- locationName
- communityName
- eventId

### 3. New endpoint to get next 3 events after upcoming
**URL: /api/events/next**

Response structure:
List of objects that have:
- name
- description
- startDate, but just hour, minutes, day, month, year
- endDate hour, minutes, day, month, year
- typeName
- communityName
- eventId

#### BONUS: Implement a simple cache
Don't make the request every time. Maybe you can save the results in a homemade  cache.
But try to invalidate them if they're too old.





